# Роль устанавливает последнюю версию Grafana Loki с GitHub.

Используются следующие параметры

    loki_user: loki
    loki_group: loki
    loki_data_dir: /opt/loki
    loki_conf_dir: /etc/loki
    loki_tmp_dir: /tmp
    loki_bin: /usr/local/bin/loki
    loki_bind_port: 3100
